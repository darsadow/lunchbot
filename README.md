# LunchBot
Classical chat webhook handler written with Spring Boot and Axon Framework

To keep lunch ordering process as nice as possible ;)

## Integrations

* HipChat

## UI & API

Currently Places and adding menu positions are functioning as API only. Soon-ish there should semi-nice interface to do that.

## List of commands

|Command  | Description |
|---------|------|
|cancel   | Cancels user order.|
|help     | Shows this help.|
|last     | Shows summary of last completed order.|
|menu     | Shows menu of a place integrated with chat room|
|list     | Shows status of current ordering process - who ordered what.|
|complete | Completes ordering process. Order summary is showed and notification is sent if configured.|
|close | Closes ordering process. No action is performed.|
|open | Opens ordering process. Since this moment users can start ordering|
|order {menu acronym / dish name}| Adds user order to the process. Subsequent calls will replace user order.|

### Development

Use https://ngrok.com/ for tunnelling from chat service to your localhost.

Provided docker-compose with MySql instance. Application container will come in the future.
 
All the rest is as in typical Spring Boot app.

Enjoy ;)
