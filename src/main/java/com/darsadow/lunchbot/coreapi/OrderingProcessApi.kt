package com.darsadow.lunchbot.coreapi

import org.axonframework.commandhandling.TargetAggregateIdentifier
import java.time.LocalDateTime

class OpenOrderingProcessCommand(@TargetAggregateIdentifier val identifier: String, val placeIdentifier: String, val roomIdentifier: String, val userIdentifier: String)
class PlaceOrderCommand(@TargetAggregateIdentifier val orderingProcessIdentifier: String, val dishIdentifier: String, val userIdentifier: String)
class CancelOrderCommand(@TargetAggregateIdentifier val orderingProcessIdentifier: String, val userIdentifier: String)
class CloseOrderingProcessCommand(@TargetAggregateIdentifier val orderingProcessIdentifier: String, val userIdentifier: String)
class CompleteOrderingProcessCommand(@TargetAggregateIdentifier val orderingProcessIdentifier: String, val completedAt: LocalDateTime, val userIdentifier: String)

class OrderingProcessOpenedEvent(val identifier: String, val placeIdentifier: String, val roomIdentifier: String, val userIdentifier: String)
class OrderPlacedEvent(val orderingProcessIdentifier: String, val dishIdentifier: String, val userIdentifier: String)
class OrderCancelledEvent(val orderingProcessIdentifier: String, val userIdentifier: String)
class OrderingProcessClosedEvent(val orderingProcessIdentifier: String, val userIdentifier: String)
class OrderingProcessCompletedEvent(
        val orderingProcessIdentifier: String,
        val placeIdentifier: String,
        val orderItems: Map<String, String>,
        val completedAt: LocalDateTime,
        val userIdentifier: String
)
