package com.darsadow.lunchbot.coreapi

import org.axonframework.commandhandling.TargetAggregateIdentifier

class CreatePlaceCommand(@TargetAggregateIdentifier val identifier: String, val accountIdentifier: String, val name: String)
class ChangePlaceNameCommand(@TargetAggregateIdentifier val identifier: String, val newName: String)
class SetMenuItemCommand(@TargetAggregateIdentifier val placeIdentifier: String, val alias: String, val dishName: String)

class PlaceCreatedEvent(val identifier: String, val accountIdentifier: String, val name: String)
class PlaceNameChangedEvent(val identifier: String, val newName: String)
class MenuItemAddedEvent(val placeIdentifier: String, val alias: String, val dishName: String)
class MenuItemUpdatedEvent(val placeIdentifier: String, val alias: String, val dishName: String)
