package com.darsadow.lunchbot.rest.controller

import org.hibernate.validator.constraints.Length
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import javax.validation.constraints.NotNull

class CreatePlaceRequest(@field:NotNull @field:Length(min = 1, max = 255) var name: String? = null)
class UpdatePlaceRequest(@field:NotNull @field:Length(min = 1, max = 255) var name: String? = null)
class AddOrChangeMenuItemRequest(
        @field:NotNull @field:Length(min = 1, max = 255) var alias: String? = null,
        @field:NotNull @field:Length(min = 1, max = 255) var name: String? = null
)

class IdentifierResponse(val id: String)

@ResponseStatus(HttpStatus.BAD_REQUEST)
class BadRequestException(msg : String) : Exception(msg)

@ResponseStatus(HttpStatus.NOT_FOUND)
class NotFoundException(msg : String) : Exception(msg)
