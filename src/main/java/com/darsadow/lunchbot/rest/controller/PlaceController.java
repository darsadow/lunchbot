package com.darsadow.lunchbot.rest.controller;

import com.darsadow.lunchbot.coreapi.SetMenuItemCommand;
import com.darsadow.lunchbot.coreapi.ChangePlaceNameCommand;
import com.darsadow.lunchbot.coreapi.CreatePlaceCommand;
import com.darsadow.lunchbot.query.Place;
import com.darsadow.lunchbot.query.PlaceRepository;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/places")
public class PlaceController {

    private CommandGateway commandGateway;

    private PlaceRepository placeRepository;

    public PlaceController(CommandGateway commandGateway, PlaceRepository placeRepository) {
        this.commandGateway = commandGateway;
        this.placeRepository = placeRepository;
    }

    @GetMapping
    public List<Place> list() throws BadRequestException {
        return placeRepository.findAll();
    }

    @PostMapping
    public IdentifierResponse create(@Valid @RequestBody CreatePlaceRequest request) throws BadRequestException {
        CompletableFuture<String> placeIdFuture = commandGateway.send(
                new CreatePlaceCommand(
                    UUID.randomUUID().toString(),
                    "user",
                    request.getName()
                )
        );

        try {
            return new IdentifierResponse(placeIdFuture.get());
        } catch (InterruptedException | ExecutionException e) {
            throw new BadRequestException(e.getMessage());
        }
    }

    @PutMapping("/{placeId}")
    public Place updateName(@PathVariable String placeId, @Valid @RequestBody UpdatePlaceRequest request) {
        commandGateway.send(new ChangePlaceNameCommand(placeId, request.getName()));

        return placeRepository.findOne(placeId);
    }

    @GetMapping("/{placeId}")
    public Place get(@PathVariable String placeId) {
        return placeRepository.findOne(placeId);
    }

    @PostMapping("/{placeId}/menu")
    public void addMenuItem(@PathVariable String placeId, @Valid @RequestBody AddOrChangeMenuItemRequest request) throws NotFoundException {
        Place place = placeRepository.findOne(placeId);

        if (place == null) throw new NotFoundException("Requested place doesn't exists");

        commandGateway.send(
                new SetMenuItemCommand(placeId, request.getAlias(), request.getName())
        );
    }
}
