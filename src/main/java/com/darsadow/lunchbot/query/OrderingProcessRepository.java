package com.darsadow.lunchbot.query;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderingProcessRepository extends JpaRepository<OrderingProcess, String> {
    Optional<OrderingProcess> findByRoomIdentifierAndStatus(String roomIdentifier, OrderingProcess.Status status);

    Optional<OrderingProcess> findFirstByRoomIdentifierOrderByCompletedAtDesc(String roomIdentifier);
}
