package com.darsadow.lunchbot.query;

import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Map;

@Entity
@NoArgsConstructor
public class Place {
    @Id
    private String id;

    @Column(name = "account_id")
    private String accountId;

    @Column
    private String name;

    @Column
    @ElementCollection
    private Map<String, String> menu;

    public Place(String id, String accountId, String name) {
        this.id = id;
        this.accountId = accountId;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getMenu() {
        return menu;
    }

    public void setMenu(Map<String, String> menu) {
        this.menu = menu;
    }
}
