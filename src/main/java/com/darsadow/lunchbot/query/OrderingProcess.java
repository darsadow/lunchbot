package com.darsadow.lunchbot.query;

import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Entity
@NoArgsConstructor
public class OrderingProcess {

    public enum Status {
        OPEN, CLOSED, COMPLETED
    }

    @Id
    private String id;

    @Column
    private String placeIdentifier;

    @Column
    private String roomIdentifier;

    @Column
    private String creatorUserIdentifier;

    @Column
    private Status status;

    @Column
    private LocalDateTime completedAt = null;

    @Column
    @ElementCollection
    private Map<String, String> orders = new HashMap<>();

    public OrderingProcess(String id, String placeIdentifier, String roomIdentifier, String creatorUserIdentifier, Status status) {
        this.id = id;
        this.placeIdentifier = placeIdentifier;
        this.roomIdentifier = roomIdentifier;
        this.creatorUserIdentifier = creatorUserIdentifier;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public String getPlaceIdentifier() {
        return placeIdentifier;
    }

    public String getRoomIdentifier() {
        return roomIdentifier;
    }

    public String getCreatorUserIdentifier() {
        return creatorUserIdentifier;
    }

    public Status getStatus() {
        return status;
    }

    public LocalDateTime getCompletedAt() {
        return completedAt;
    }

    public Map<String, String> getOrders() {
        return orders;
    }

    void close() {
        status = Status.CLOSED;
    }

    void complete(LocalDateTime completedAt) {
        status = Status.COMPLETED;
        this.completedAt = completedAt;
    }
}
