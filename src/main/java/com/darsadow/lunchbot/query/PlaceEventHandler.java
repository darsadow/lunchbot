package com.darsadow.lunchbot.query;

import com.darsadow.lunchbot.coreapi.MenuItemAddedEvent;
import com.darsadow.lunchbot.coreapi.MenuItemUpdatedEvent;
import com.darsadow.lunchbot.coreapi.PlaceCreatedEvent;
import com.darsadow.lunchbot.coreapi.PlaceNameChangedEvent;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

@Component
public class PlaceEventHandler {

    private PlaceRepository placeRepository;

    public PlaceEventHandler(PlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
    }

    @EventHandler
    public void on(PlaceCreatedEvent event) {
        Place place = new Place(event.getIdentifier(), event.getAccountIdentifier(), event.getName());

        placeRepository.save(place);
    }

    @EventHandler
    public void on(PlaceNameChangedEvent event) {
        Place place = placeRepository.findOne(event.getIdentifier());
        place.setName(event.getNewName());

        placeRepository.save(place);
    }

    @EventHandler
    public void on(MenuItemAddedEvent event) {
        Place place = placeRepository.findOne(event.getPlaceIdentifier());
        place.getMenu().put(event.getAlias(), event.getDishName());

        placeRepository.save(place);
    }

    @EventHandler
    public void on(MenuItemUpdatedEvent event) {
        Place place = placeRepository.findOne(event.getPlaceIdentifier());
        place.getMenu().replace(event.getAlias(), event.getDishName());

        placeRepository.save(place);
    }
}
