package com.darsadow.lunchbot.query;

import com.darsadow.lunchbot.coreapi.*;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class OrderingProcessEventListener {

    private OrderingProcessRepository orderingProcessRepository;

    public OrderingProcessEventListener(OrderingProcessRepository orderingProcessRepository) {
        this.orderingProcessRepository = orderingProcessRepository;
    }

    @EventHandler
    public void on(OrderingProcessOpenedEvent event) {
        OrderingProcess process = new OrderingProcess(
                event.getIdentifier(),
                event.getPlaceIdentifier(),
                event.getRoomIdentifier(),
                event.getUserIdentifier(),
                OrderingProcess.Status.OPEN
        );

        orderingProcessRepository.save(process);
    }

    @EventHandler
    public void on(OrderPlacedEvent event) {
        OrderingProcess process = orderingProcessRepository.findOne(event.getOrderingProcessIdentifier());

        process.getOrders().put(event.getUserIdentifier(), event.getDishIdentifier());

        orderingProcessRepository.save(process);
    }

    @EventHandler
    public void on(OrderCancelledEvent event) {
        OrderingProcess process = orderingProcessRepository.findOne(event.getOrderingProcessIdentifier());

        process.getOrders().remove(event.getUserIdentifier());

        orderingProcessRepository.save(process);
    }

    @EventHandler
    public void on(OrderingProcessClosedEvent event) {
        OrderingProcess process = orderingProcessRepository.findOne(event.getOrderingProcessIdentifier());

        process.close();

        orderingProcessRepository.save(process);
    }

    @EventHandler
    public void on(OrderingProcessCompletedEvent event) {
        OrderingProcess process = orderingProcessRepository.findOne(event.getOrderingProcessIdentifier());

        process.complete(LocalDateTime.now());

        orderingProcessRepository.save(process);
    }
}
