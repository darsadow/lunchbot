package com.darsadow.lunchbot.place;

import com.darsadow.lunchbot.coreapi.*;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.HashMap;
import java.util.Map;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

@Aggregate
@NoArgsConstructor
public class Place {

    @AggregateIdentifier
    private String identifier;
    private String accountIdentifier;
    private String name;
    private Map<String, String> menu = new HashMap<>();

    @CommandHandler
    public Place(CreatePlaceCommand command) {
        apply(new PlaceCreatedEvent(command.getIdentifier(), command.getAccountIdentifier(), command.getName()));
    }

    @CommandHandler
    public void handle(ChangePlaceNameCommand command) {
        if (!name.equals(command.getNewName())) {
            apply(new PlaceNameChangedEvent(command.getIdentifier(), command.getNewName()));
        }
    }

    @CommandHandler
    public void handle(SetMenuItemCommand command) {
        if (menu.containsKey(command.getAlias())) {
            apply(new MenuItemUpdatedEvent(command.getPlaceIdentifier(), command.getAlias(), command.getDishName()));
        } else {
            apply(new MenuItemAddedEvent(command.getPlaceIdentifier(), command.getAlias(), command.getDishName()));
        }
    }

    @EventSourcingHandler
    public void on(PlaceCreatedEvent event) {
        identifier = event.getIdentifier();
        accountIdentifier = event.getAccountIdentifier();
        name = event.getName();
    }

    @EventSourcingHandler
    public void on(PlaceNameChangedEvent event) {
        name = event.getNewName();
    }

    @EventSourcingHandler
    public void on(MenuItemAddedEvent event) {
        menu.put(event.getAlias(), event.getDishName());
    }

    @EventSourcingHandler
    public void on(MenuItemUpdatedEvent event) {
        menu.replace(event.getAlias(), event.getDishName());
    }
}
