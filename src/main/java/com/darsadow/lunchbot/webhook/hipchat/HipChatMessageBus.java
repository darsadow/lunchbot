package com.darsadow.lunchbot.webhook.hipchat;

import com.darsadow.lunchbot.messagebus.HandlerNotFoundException;
import com.darsadow.lunchbot.messagebus.Message;
import com.darsadow.lunchbot.messagebus.MessageBus;
import com.darsadow.lunchbot.messasgehandler.*;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.stream.Collectors;

@Component
public class HipChatMessageBus {
    private MessageBus messageBus;

    public HipChatMessageBus(MessageBus messageBus) {
        this.messageBus = messageBus;
    }

    HipChatResponse handle(Message message) {
        try {
            Object result = messageBus.handle(message);

            if (result instanceof Menu) {
                String formattedMenu = "<strong>" + ((Menu) result).getPlaceName() + "</strong><br />"
                        + ((Menu) result).getItems().entrySet().stream()
                        .map(map -> map.getValue() + " - " + map.getKey())
                        .collect(Collectors.joining("<br />"));

                return HipChatResponse.of.messageAndColor(formattedMenu, "green");
            }

            if (result instanceof OrderingOpen) {
                return HipChatResponse.of.messageAndColor("You can now place your orders", "green");
            }
            if (result instanceof OrderingAlreadyOpen) {
                return HipChatResponse.of.messageAndColor("Ordering already open", "red");
            }

            if (result instanceof OrderAccepted) {
                return HipChatResponse.of.messageAndColor(
                        "Order for @" + ((OrderAccepted) result).getUserIdentifier() + " accepted",
                        "green"
                );
            }

            if (result instanceof OrderNotFound) {
                return HipChatResponse.of.messageAndColor(
                        "Order for @" + ((OrderNotFound) result).getUserIdentifier() + " not found",
                        "red"
                );
            }

            if (result instanceof OrderCancelled) {
                return HipChatResponse.of.messageAndColor(
                        "Order for @" + ((OrderCancelled) result).getUserIdentifier() + " cancelled",
                        "yellow"
                );
            }

            if (result instanceof OrderingProcessClosed) {
                return HipChatResponse.of.messageAndColor(
                        "Ordering process closed. No orders will be accepted.",
                        "yellow"
                );
            }

            if (result instanceof OrderList) {
                String formattedOrders = "<strong>" + ((OrderList) result).getPlaceName() + "</strong><br />"
                        + ((OrderList) result).getOrders().entrySet().stream()
                        .map(map -> map.getKey() + " - " + map.getValue())
                        .collect(Collectors.joining("<br />"));

                return HipChatResponse.of.messageAndColor(formattedOrders, "green");
            }

            if (result instanceof OrderingProcessNotFound) {
                return HipChatResponse.of.messageAndColor(
                        "Ordering process is not open.",
                        "red"
                );
            }

            if (result instanceof OrderingProcessCompleted) {
                String formattedOrders = "<strong>Ordering completed for " + ((OrderingProcessCompleted) result).getPlaceName() + "</strong><br />"
                        + ((OrderingProcessCompleted) result).getOrders().entrySet().stream()
                        .map(Map.Entry::getValue)
                        .collect(Collectors.groupingBy(e -> e, Collectors.counting()))
                        .entrySet().stream()
                        .map(orderLine -> orderLine.getKey() + ": " + orderLine.getValue())
                        .collect(Collectors.joining("<br />"));

                return HipChatResponse.of.messageAndColor(formattedOrders, "green");
            }

            if (result instanceof LastOrderingProcessCompleted) {
                String formattedOrders = "<strong>Ordering completed for "
                        + ((LastOrderingProcessCompleted) result).getPlaceName() + " at "
                        + ((LastOrderingProcessCompleted) result).getCompletedAt().toString() + "</strong><br />"
                        + ((LastOrderingProcessCompleted) result).getOrders().entrySet().stream()
                        .map(Map.Entry::getValue)
                        .collect(Collectors.groupingBy(e -> e, Collectors.counting()))
                        .entrySet().stream()
                        .map(orderLine -> orderLine.getKey() + ": " + orderLine.getValue())
                        .collect(Collectors.joining("<br />"));

                return HipChatResponse.of.messageAndColor(formattedOrders, "green");
            }

            if (result instanceof Help) {
                String formattedHelp = ((Help) result).getCommands().entrySet().stream()
                        .map(command -> command.getKey() + " - " + command.getValue())
                        .collect(Collectors.joining("<br />"));

                return HipChatResponse.of.messageAndColor(
                        "<strong>" + ((Help) result).getHeader() + "</strong><br />" + formattedHelp,
                        "green"
                );
            }

            if (result instanceof UserDoesNotMatch) {
                return HipChatResponse.of.messageAndColor(
                        "Only @" + ((UserDoesNotMatch) result).getOwnerUserIdentifier() + " can perform this operation.",
                        "red"
                );
            }

            return HipChatResponse.of.messageAndColor("Message accepted. Probably ...", "yellow");
        } catch (HandlerNotFoundException ex) {
            return HipChatResponse.of.messageAndColor("Unknown command. Use \"help\" to see full list of commands.", "yellow");
        } catch (Exception ex) {
            return HipChatResponse.of.messageAndColor(ex.getMessage(), "red");
        }
    }
}
