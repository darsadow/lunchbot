package com.darsadow.lunchbot.webhook.hipchat

import com.fasterxml.jackson.annotation.JsonProperty

class HipChatRequest(val event: String, val item: Item, val webhookId: Int)
class Item(val message: Message, val room: Room)
class Message(val date: String, val from: User, val id: String, val message: String, val type: String)
class User(val id: Int, @field:JsonProperty("mention_name") val mentionName: String, val name: String)
class Room(val id: Int, val name: String)

class HipChatResponse(val message: String, val color: String, var notify: Boolean, @field:JsonProperty("message_format") val messageFormat: String) {
    companion object of {
        fun messageAndColor(message: String, color: String) : HipChatResponse {
            return HipChatResponse(message, color, false, "html");
        }
    }
}
