package com.darsadow.lunchbot.webhook.hipchat;

import com.darsadow.lunchbot.messagebus.Message;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WebhookController {

    private HipChatMessageBus messageBus;

    public WebhookController(HipChatMessageBus messageBus) {
        this.messageBus = messageBus;
    }

    @PostMapping("/hipchat/{placeId}/{accountId}")
    public HipChatResponse webhookHandler(@PathVariable String placeId, @PathVariable String accountId,
                                          @RequestBody HipChatRequest request) {
        Message message = new Message(
                parseCommand(request.getItem().getMessage().getMessage()),
                parseArgument(request.getItem().getMessage().getMessage()),
                request.getItem().getMessage().getFrom().getMentionName(),
                placeId,
                String.valueOf(request.getItem().getRoom().getId()),
                accountId
        );

        return messageBus.handle(message);
    }

    private String parseArgument(String message) {
        String[] split = message.split(" ", 3);

        if (split.length == 3) {
            return split[2];
        }

        return "";
    }

    private String parseCommand(String message) {
        String[] split = message.split(" ");

        if (split.length > 1) {
            return split[1];
        }

        return "";
    }
}
