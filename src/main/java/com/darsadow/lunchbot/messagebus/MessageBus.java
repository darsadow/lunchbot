package com.darsadow.lunchbot.messagebus;

import java.util.HashMap;
import java.util.Map;

public class MessageBus {
    private Map<String, MessageHandler> handlers = new HashMap<>();

    public Object handle(Message message) {
        if (!handlers.containsKey(message.getCommand())) {
            throw new HandlerNotFoundException("Handler for " + message.getCommand() + " does not exist");
        }

        return handlers.get(message.getCommand()).handle(message);
    }

    public void registerHandler(String command, MessageHandler handler) {
        if (handlers.containsKey(command)) {
            throw new RuntimeException("Handler for " + command + " already exist");
        }

        handlers.put(command, handler);
    }
}
