package com.darsadow.lunchbot.messagebus

class Message(val command: String,
              val arguments: String,
              val senderUserIdentifier: String,
              val placeIdentifier: String,
              val roomIdentifier: String,
              val accountIdentifier: String
)
