package com.darsadow.lunchbot.messagebus;

public interface MessageHandler {
    Object handle(Message message);
}
