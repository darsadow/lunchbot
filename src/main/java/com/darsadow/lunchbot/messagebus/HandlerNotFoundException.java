package com.darsadow.lunchbot.messagebus;

public class HandlerNotFoundException extends RuntimeException {
    HandlerNotFoundException(String message) {
        super(message);
    }
}
