package com.darsadow.lunchbot;

import org.axonframework.spring.config.EnableAxon;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAxon
public class LunchbotApplication {

	public static void main(String[] args) {
		SpringApplication.run(LunchbotApplication.class, args);
	}
}
