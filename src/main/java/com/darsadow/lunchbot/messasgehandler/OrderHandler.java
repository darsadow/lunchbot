package com.darsadow.lunchbot.messasgehandler;

import com.darsadow.lunchbot.coreapi.PlaceOrderCommand;
import com.darsadow.lunchbot.messagebus.Message;
import com.darsadow.lunchbot.messagebus.MessageHandler;
import com.darsadow.lunchbot.query.OrderingProcess;
import com.darsadow.lunchbot.query.OrderingProcessRepository;
import com.darsadow.lunchbot.query.Place;
import com.darsadow.lunchbot.query.PlaceRepository;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OrderHandler implements MessageHandler {

    private CommandGateway commandGateway;

    private OrderingProcessRepository orderingProcessRepository;

    private PlaceRepository placeRepository;

    public OrderHandler(CommandGateway commandGateway, OrderingProcessRepository orderingProcessRepository, PlaceRepository placeRepository) {
        this.commandGateway = commandGateway;
        this.orderingProcessRepository = orderingProcessRepository;
        this.placeRepository = placeRepository;
    }

    @Override
    public Object handle(Message message) {

        Optional<OrderingProcess> orderingProcess = orderingProcessRepository.findByRoomIdentifierAndStatus(
                message.getRoomIdentifier(),
                OrderingProcess.Status.OPEN
        );

        if (!orderingProcess.isPresent()) {
            return new OrderingProcessNotFound();
        }

        Place place = placeRepository.findOne(message.getPlaceIdentifier());

        PlaceOrderCommand command = new PlaceOrderCommand(
                orderingProcess.get().getId(),
                place.getMenu().getOrDefault(message.getArguments(), message.getArguments()),
                message.getSenderUserIdentifier()
        );

        commandGateway.send(command);

        return new OrderAccepted(message.getSenderUserIdentifier());
    }
}
