package com.darsadow.lunchbot.messasgehandler;

import com.darsadow.lunchbot.coreapi.CancelOrderCommand;
import com.darsadow.lunchbot.messagebus.Message;
import com.darsadow.lunchbot.messagebus.MessageHandler;
import com.darsadow.lunchbot.query.OrderingProcess;
import com.darsadow.lunchbot.query.OrderingProcessRepository;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CancelHandler implements MessageHandler {

    private CommandGateway commandGateway;

    private OrderingProcessRepository orderingProcessRepository;

    public CancelHandler(CommandGateway commandGateway, OrderingProcessRepository orderingProcessRepository) {
        this.commandGateway = commandGateway;
        this.orderingProcessRepository = orderingProcessRepository;
    }

    @Override
    public Object handle(Message message) {
        Optional<OrderingProcess> orderingProcess = orderingProcessRepository.findByRoomIdentifierAndStatus(
                message.getRoomIdentifier(),
                OrderingProcess.Status.OPEN
        );

        if (!orderingProcess.isPresent()) {
            return new OrderingProcessNotFound();
        }

        if (!orderingProcess.get().getOrders().containsKey(message.getSenderUserIdentifier())) {
            return new OrderNotFound(message.getSenderUserIdentifier());
        }

        commandGateway.send(new CancelOrderCommand(orderingProcess.get().getId(), message.getSenderUserIdentifier()));

        return new OrderCancelled(message.getSenderUserIdentifier());
    }
}
