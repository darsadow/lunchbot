package com.darsadow.lunchbot.messasgehandler;

import com.darsadow.lunchbot.messagebus.Message;
import com.darsadow.lunchbot.messagebus.MessageHandler;
import com.darsadow.lunchbot.query.OrderingProcess;
import com.darsadow.lunchbot.query.OrderingProcessRepository;
import com.darsadow.lunchbot.query.Place;
import com.darsadow.lunchbot.query.PlaceRepository;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ListHandler implements MessageHandler{

    private OrderingProcessRepository orderingProcessRepository;

    private PlaceRepository placeRepository;

    public ListHandler(OrderingProcessRepository orderingProcessRepository, PlaceRepository placeRepository) {
        this.orderingProcessRepository = orderingProcessRepository;
        this.placeRepository = placeRepository;
    }

    @Override
    public Object handle(Message message) {
        Optional<OrderingProcess> orderingProcess = orderingProcessRepository.findByRoomIdentifierAndStatus(
                message.getRoomIdentifier(),
                OrderingProcess.Status.OPEN
        );

        if (!orderingProcess.isPresent()) {
            return new OrderingProcessNotFound();
        }

        Place place = placeRepository.findOne(message.getPlaceIdentifier());

        return new OrderList(place.getName(), orderingProcess.get().getOrders());
    }
}
