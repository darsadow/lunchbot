package com.darsadow.lunchbot.messasgehandler

import java.time.LocalDateTime

class Menu(val placeName: String, val items: Map<String, String>)
class OrderingOpen
class OrderingAlreadyOpen

class OrderAccepted(val userIdentifier: String)
class OrderCancelled(val userIdentifier: String)
class OrderNotFound(val userIdentifier: String)
class OrderingProcessClosed
class OrderList(val placeName: String, val orders: Map<String, String>)
class OrderingProcessCompleted(val placeName: String, val orders: Map<String, String>)
class NoOrderingProcessCompleted
class LastOrderingProcessCompleted(val placeName: String, val orders: Map<String, String>, val completedAt: LocalDateTime)

class OrderingProcessNotFound
class UserDoesNotMatch(val ownerUserIdentifier: String, val requestingUserIdentifier: String)

class Help(val header: String, val commands: Map<String, String>)
