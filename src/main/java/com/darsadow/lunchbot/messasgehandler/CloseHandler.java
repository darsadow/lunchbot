package com.darsadow.lunchbot.messasgehandler;

import com.darsadow.lunchbot.coreapi.CloseOrderingProcessCommand;
import com.darsadow.lunchbot.messagebus.Message;
import com.darsadow.lunchbot.messagebus.MessageHandler;
import com.darsadow.lunchbot.query.OrderingProcess;
import com.darsadow.lunchbot.query.OrderingProcessRepository;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CloseHandler implements MessageHandler {

    private CommandGateway commandGateway;

    private OrderingProcessRepository orderingProcessRepository;

    public CloseHandler(CommandGateway commandGateway, OrderingProcessRepository orderingProcessRepository) {
        this.commandGateway = commandGateway;
        this.orderingProcessRepository = orderingProcessRepository;
    }

    @Override
    public Object handle(Message message) {
        Optional<OrderingProcess> orderingProcess = orderingProcessRepository.findByRoomIdentifierAndStatus(
                message.getRoomIdentifier(),
                OrderingProcess.Status.OPEN
        );

        if (!orderingProcess.isPresent()) {
            return new OrderingProcessNotFound();
        }

        if (!orderingProcess.get().getCreatorUserIdentifier().equals(message.getSenderUserIdentifier())) {
            return new UserDoesNotMatch(orderingProcess.get().getCreatorUserIdentifier(), message.getSenderUserIdentifier());
        }

        commandGateway.send(
                new CloseOrderingProcessCommand(orderingProcess.get().getId(), message.getSenderUserIdentifier())
        );

        return new OrderingProcessClosed();
    }
}
