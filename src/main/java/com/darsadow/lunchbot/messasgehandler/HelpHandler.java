package com.darsadow.lunchbot.messasgehandler;

import com.darsadow.lunchbot.messagebus.Message;
import com.darsadow.lunchbot.messagebus.MessageHandler;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class HelpHandler implements MessageHandler {
    @Override
    public Object handle(Message message) {
        Map<String, String> commands = new LinkedHashMap<>();
        commands.put("help", "Shows this help.");
        commands.put("menu", "Shows menu of a place integrated with chat room");
        commands.put("open", "Opens ordering process. Since this moment users can start ordering");
        commands.put("order {menu acronym | dish name}", "Adds user order to the process. Subsequent calls will replace user order.");
        commands.put("cancel", "Cancels user order.");
        commands.put("list", "Shows status of current ordering process - who ordered what.");
        commands.put("complete", "Completes ordering process. Order summary is showed and notification is sent if configured.");
        commands.put("close", "Closes ordering process. No action is performed.");
        commands.put("last", "Shows summary of last completed order.");

        String header = "LunchBot - list of commands";

        return new Help(header, commands);
    }
}
