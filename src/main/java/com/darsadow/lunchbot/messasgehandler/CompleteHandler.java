package com.darsadow.lunchbot.messasgehandler;

import com.darsadow.lunchbot.coreapi.CompleteOrderingProcessCommand;
import com.darsadow.lunchbot.messagebus.Message;
import com.darsadow.lunchbot.messagebus.MessageHandler;
import com.darsadow.lunchbot.query.OrderingProcess;
import com.darsadow.lunchbot.query.OrderingProcessRepository;
import com.darsadow.lunchbot.query.Place;
import com.darsadow.lunchbot.query.PlaceRepository;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;

@Component
public class CompleteHandler implements MessageHandler {

    private CommandGateway commandGateway;

    private OrderingProcessRepository orderingProcessRepository;

    private PlaceRepository placeRepository;

    public CompleteHandler(CommandGateway commandGateway, OrderingProcessRepository orderingProcessRepository, PlaceRepository placeRepository) {
        this.commandGateway = commandGateway;
        this.orderingProcessRepository = orderingProcessRepository;
        this.placeRepository = placeRepository;
    }

    @Override
    public Object handle(Message message) {
        Optional<OrderingProcess> orderingProcess = orderingProcessRepository.findByRoomIdentifierAndStatus(
                message.getRoomIdentifier(),
                OrderingProcess.Status.OPEN
        );

        if (!orderingProcess.isPresent()) {
            return new OrderingProcessNotFound();
        }

        if (!orderingProcess.get().getCreatorUserIdentifier().equals(message.getSenderUserIdentifier())) {
            return new UserDoesNotMatch(orderingProcess.get().getCreatorUserIdentifier(), message.getSenderUserIdentifier());
        }

        commandGateway.send(
                new CompleteOrderingProcessCommand(
                        orderingProcess.get().getId(),
                        LocalDateTime.now(),
                        message.getSenderUserIdentifier()
                )
        );

        Place place = placeRepository.findOne(message.getPlaceIdentifier());

        return new OrderingProcessCompleted(place.getName(), orderingProcess.get().getOrders());
    }
}
