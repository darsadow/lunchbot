package com.darsadow.lunchbot.messasgehandler;

import com.darsadow.lunchbot.messagebus.Message;
import com.darsadow.lunchbot.messagebus.MessageHandler;
import com.darsadow.lunchbot.query.Place;
import com.darsadow.lunchbot.query.PlaceRepository;
import org.springframework.stereotype.Component;

@Component
public class MenuHandler implements MessageHandler {

    private PlaceRepository placeRepository;

    public MenuHandler(PlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
    }

    @Override
    public Object handle(Message message) {
        Place place = placeRepository.findOne(message.getPlaceIdentifier());

        return new Menu(place.getName(), place.getMenu());
    }
}
