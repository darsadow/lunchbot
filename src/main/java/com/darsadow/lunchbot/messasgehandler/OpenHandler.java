package com.darsadow.lunchbot.messasgehandler;

import com.darsadow.lunchbot.coreapi.OpenOrderingProcessCommand;
import com.darsadow.lunchbot.messagebus.Message;
import com.darsadow.lunchbot.messagebus.MessageHandler;
import com.darsadow.lunchbot.query.OrderingProcess;
import com.darsadow.lunchbot.query.OrderingProcessRepository;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Component
public class OpenHandler implements MessageHandler {

    private CommandGateway commandGateway;

    private OrderingProcessRepository orderingProcessRepository;

    public OpenHandler(CommandGateway commandGateway, OrderingProcessRepository orderingProcessRepository) {
        this.commandGateway = commandGateway;
        this.orderingProcessRepository = orderingProcessRepository;
    }

    @Override
    public Object handle(Message message) {
        Optional<OrderingProcess> openOrderingProcess = orderingProcessRepository.findByRoomIdentifierAndStatus(
                message.getRoomIdentifier(),
                OrderingProcess.Status.OPEN
        );

        if (openOrderingProcess.isPresent()) {
            return new OrderingAlreadyOpen();
        }

        commandGateway.send(
                new OpenOrderingProcessCommand(
                        UUID.randomUUID().toString(),
                        message.getPlaceIdentifier(),
                        message.getRoomIdentifier(),
                        message.getSenderUserIdentifier()
                )
        );

        return new OrderingOpen();
    }
}
