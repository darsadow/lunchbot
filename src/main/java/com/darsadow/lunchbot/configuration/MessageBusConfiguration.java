package com.darsadow.lunchbot.configuration;

import com.darsadow.lunchbot.messagebus.MessageBus;
import com.darsadow.lunchbot.messasgehandler.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageBusConfiguration {

    private ApplicationContext context;

    public MessageBusConfiguration(ApplicationContext context) {
        this.context = context;
    }

    @Bean
    public MessageBus chatBus() {
        MessageBus bus = new MessageBus();

        bus.registerHandler("menu", context.getBean(MenuHandler.class));
        bus.registerHandler("open", context.getBean(OpenHandler.class));
        bus.registerHandler("order", context.getBean(OrderHandler.class));
        bus.registerHandler("cancel", context.getBean(CancelHandler.class));
        bus.registerHandler("close", context.getBean(CloseHandler.class));
        bus.registerHandler("list", context.getBean(ListHandler.class));
        bus.registerHandler("complete", context.getBean(CompleteHandler.class));
        bus.registerHandler("last", context.getBean(LastHandler.class));
        bus.registerHandler("help", context.getBean(HelpHandler.class));

        return bus;
    }
}
