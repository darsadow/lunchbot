package com.darsadow.lunchbot.orderingprocess;

import com.darsadow.lunchbot.coreapi.*;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.model.AggregateIdentifier;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.HashMap;
import java.util.Map;

import static org.axonframework.commandhandling.model.AggregateLifecycle.apply;

@Aggregate
@NoArgsConstructor
public class OrderingProcess {

    private enum Status {
        OPEN, CLOSED
    }

    @AggregateIdentifier
    private String identifier;
    private String placeIdentifier;
    private String roomIdentifier;
    private String userIdentifier;
    private Status status;
    private Map<String, String> orders = new HashMap<>();

    @CommandHandler
    public OrderingProcess(OpenOrderingProcessCommand command) {
        apply(new OrderingProcessOpenedEvent(
                command.getIdentifier(),
                command.getPlaceIdentifier(),
                command.getRoomIdentifier(),
                command.getUserIdentifier()
            )
        );
    }

    @CommandHandler
    public void handle(PlaceOrderCommand command) throws OrderingProcessClosedException {
        if (status.equals(Status.CLOSED)) {
            throw new OrderingProcessClosedException();
        }

        apply(new OrderPlacedEvent(command.getOrderingProcessIdentifier(), command.getDishIdentifier(), command.getUserIdentifier()));
    }

    @CommandHandler
    public void handle(CancelOrderCommand command) throws OrderDoesNotExistsException, OrderingProcessClosedException {
        if (status.equals(Status.CLOSED)) {
            throw new OrderingProcessClosedException();
        }

        if (!orders.containsKey(command.getUserIdentifier())) {
            throw new OrderDoesNotExistsException();
        }

        apply(new OrderCancelledEvent(command.getOrderingProcessIdentifier(), command.getUserIdentifier()));
    }

    @CommandHandler
    public void handle(CloseOrderingProcessCommand command) throws OrderingProcessClosedException {
        if (status.equals(Status.CLOSED)) {
            throw new OrderingProcessClosedException();
        }

        if (!command.getUserIdentifier().equals(userIdentifier)) {
            throw new NotTheSameUserException();
        }

        apply(new OrderingProcessClosedEvent(command.getOrderingProcessIdentifier(), command.getUserIdentifier()));
    }

    @CommandHandler
    public void handle(CompleteOrderingProcessCommand command) throws OrderingProcessClosedException {
        if (status.equals(Status.CLOSED)) {
            throw new OrderingProcessClosedException();
        }

        if (!command.getUserIdentifier().equals(userIdentifier)) {
            throw new NotTheSameUserException();
        }

        if (orders.isEmpty()) {
            apply(new OrderingProcessClosedEvent(command.getOrderingProcessIdentifier(), command.getUserIdentifier()));
            return;
        }

        apply(new OrderingProcessCompletedEvent(
                command.getOrderingProcessIdentifier(),
                placeIdentifier,
                orders,
                command.getCompletedAt(),
                command.getUserIdentifier()
        ));
    }

    @EventSourcingHandler
    public void on(OrderingProcessOpenedEvent event) {
        identifier = event.getIdentifier();
        placeIdentifier = event.getPlaceIdentifier();
        roomIdentifier = event.getRoomIdentifier();
        userIdentifier = event.getUserIdentifier();
        status = Status.OPEN;
    }

    @EventSourcingHandler
    public void on(OrderPlacedEvent event) {
        orders.put(event.getUserIdentifier(), event.getDishIdentifier());
    }

    @EventSourcingHandler
    public void on(OrderCancelledEvent event) {
        orders.remove(event.getUserIdentifier());
    }

    @EventSourcingHandler
    public void on(OrderingProcessClosedEvent event) {
        status = Status.CLOSED;
    }
}
