import React from 'react';
import ReactDom from 'react-dom';
import { Router, Route, hashHistory } from 'react-router';

import Layout from './Layout'
import Home from './Home'

import PlacesList from './places/PlacesList'
import Place from './places/Place'
import NewPlace from './places/NewPlace'

document.addEventListener('DOMContentLoaded', function() {
    ReactDom.render((
            <Router history={hashHistory}>
                <Route component={Layout}>
                    <Route path="/" component={Home}/>
                    <Route path="/newplace" component={NewPlace}/>
                    <Route path="/places" component={PlacesList}/>
                    <Route path="/places/:placeId" component={Place}/>
                </Route>
            </Router>
        ),
        document.getElementById('root')
    );
});
