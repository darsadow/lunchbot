import React from 'react'

class Home extends React.Component {

    render() {
        return (
                <div className="starter-template">
                    <h1>LunchBot &lt;3</h1>
                    <p className="lead">Add and update places and menu for your LunchBot.</p>
                </div>
        );
    }
}
export default Home;
