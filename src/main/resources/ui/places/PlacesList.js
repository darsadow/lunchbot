import React from 'react'
import { Link } from 'react-router'
import axios from 'axios'

class PlacesList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            places: []
        }
    }

    componentDidMount() {
        var _this = this;
        this.serverRequest = axios
            .get('http://localhost:8080/api/places')
            .then(function (result) {
                _this.setState({
                    places: result.data
                })
            });
    }

    render() {
        return (
            <div className="starter-template">
                <h1>Places</h1>
                {this.state.places.map(function (place, i) {
                    return (
                        <li className="place-item" key={i}><Link to={"/places/" + place.id}> {place.name} </Link></li>
                    );
                })}
            </div>
        );
    }
}
export default PlacesList
