import React from 'react'

class PlaceNameForm extends React.Component {
    constructor() {
        super();
    }

    handleChange(event) {
        this.props.placeName = event.target.value;
    }

    handleSubmit(event) {
        const placeName = this.props.placeName;
        this.props.submitHandler({placeName});

        event.preventDefault();
    }

    render() {
            return (
            <form onSubmit={this.handleSubmit.bind(this)} className="form-inline">
                <div className="form-group">
                    <input value={this.props.placeName} onChange={this.props.changeHandler} className="form-control" />
                    <button type="submit" className="btn btn-success">Save</button>
                </div>
            </form>
            );
    }
}

export default PlaceNameForm
