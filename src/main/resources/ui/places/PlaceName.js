import React from 'react'

import PlaceNameForm from './PlaceNameForm'

class PlaceName extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editing: false
        }
    }

    updateNameHandler(newName) {
        console.log("updated!", newName);
        this.props.updateHandler(newName);
        this.setState({editing: false});
    }

    startEditing() {
        console.log('Staring editing');
        this.setState({editing: true});
    }

    render() {
        if (this.state.editing) {
            return (
                <PlaceNameForm submitHandler={this.updateNameHandler.bind(this)} placeName={this.props.placeName}
                    changeHandler={this.props.changeHandler} />
            );
        }

        return (
            <h1 onClick={this.startEditing.bind(this)}>{this.props.placeName}</h1>
        );
    }
}
export default PlaceName
