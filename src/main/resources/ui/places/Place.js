import React from 'react'
import axios from 'axios'

import PlaceName from './PlaceName'

class Place extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            id: '',
            menu: {}
        };

    }

    componentDidMount() {
        let _this = this;
        axios
            .get('http://localhost:8080/api/places/' + _this.props.params.placeId)
            .then(function(response) {
                _this.setState({
                    name: response.data.name,
                    id: response.data.id,
                    menu: response.data.menu
                });
            });
    }

    mapObject(object, callback) {
        if (!object) {
            object = {};
        }
        return Object.keys(object).map(function (key) {
            return callback(key, object[key]);
        });
    }

    onPlaceNameChange(event) {
        this.setState({name: event.target.value});
    }

    onPlaceNameUpdate(newName) {
        let name = newName.placeName;
        axios.put('http://localhost:8080/api/places/' + this.state.id, {name})
            .then(function (response) {
            });
    }

    render() {
        return (
            <div className="place">
                <PlaceName placeName={this.state.name} changeHandler={this.onPlaceNameChange.bind(this)}
                    updateHandler={this.onPlaceNameUpdate.bind(this)} />
                <p>Integration link:</p>
                <pre>http://localhost:8080/hipchat/{this.state.id}</pre>
                <h3>Menu:</h3>
                <table>
                    <thead>
                    <tr>
                        <th>Alias</th>
                        <th>Dish name</th>
                    </tr>
                    </thead>
                    <tbody>
                    { this.mapObject(this.state.menu, function (alias, name) {
                        return (
                            <tr key={alias}>
                                <td>{alias}</td>
                                <td>{name}</td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
            </div>
        );
    }
}
export default Place
