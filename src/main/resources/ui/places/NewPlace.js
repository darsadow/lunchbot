import React from 'react'
import axios from 'axios'

import PlaceNameForm from './PlaceNameForm'

class NewPlace extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            name: ""
        }
    }

    handleSubmit(place) {
        const _this = this;
        const name = this.state.name;
        axios.post('http://localhost:8080/api/places', {name})
            .then(function (response) {
                _this.props.router.push("/places/" + response.data.id);
            });
    }

    handleChange(event) {
        this.setState({name: event.target.value});
    }

    render() {
        return (
            <PlaceNameForm placeName={this.state.name} submitHandler={this.handleSubmit.bind(this)}
                changeHandler={this.handleChange.bind(this)}/>
        );
    }
}
export default NewPlace
