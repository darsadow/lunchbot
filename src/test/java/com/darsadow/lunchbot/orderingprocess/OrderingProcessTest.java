package com.darsadow.lunchbot.orderingprocess;

import com.darsadow.lunchbot.coreapi.*;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class OrderingProcessTest {

    private FixtureConfiguration<OrderingProcess> fixture;

    @Before
    public void setUp() throws Exception {
        fixture = new AggregateTestFixture<>(OrderingProcess.class);
    }

    @Test
    public void testOpeningOrderingProcess() throws Exception {
        fixture.givenNoPriorActivity()
                .when(new OpenOrderingProcessCommand("uuid", "placeUuid", "roomId", "userName"))
                .expectReturnValue("uuid")
                .expectEvents(new OrderingProcessOpenedEvent("uuid", "placeUuid", "roomId", "userName"));
    }

    @Test
    public void testPlacingOrder() throws Exception {
        fixture.given(new OrderingProcessOpenedEvent("uuid", "placeUuid", "roomId", "userName"))
                .when(new PlaceOrderCommand("uuid", "pppp", "user"))
                .expectVoidReturnType()
                .expectEvents(new OrderPlacedEvent("uuid", "pppp", "user"));
    }

    @Test
    public void testPlacingOrderWhenStatusIsClosed() throws Exception {
        fixture.given(
                new OrderingProcessOpenedEvent("uuid", "placeUuid", "roomId", "userName"),
                new OrderingProcessClosedEvent("uuid", "userName")
        )
                .when(new PlaceOrderCommand("uuid", "pppp", "user"))
                .expectException(OrderingProcessClosedException.class);
    }

    @Test
    public void testCancellingOrder() throws Exception {
        fixture.given(
                new OrderingProcessOpenedEvent("uuid", "placeUuid", "roomId", "userName"),
                new OrderPlacedEvent("uuid", "pppp", "user")
        )
                .when(new CancelOrderCommand("uuid", "user"))
                .expectVoidReturnType()
                .expectEvents(new OrderCancelledEvent("uuid", "user"));

    }

    @Test
    public void testCancellingOrderWhenStatusIsClosed() throws Exception {
        fixture.given(
                new OrderingProcessOpenedEvent("uuid", "placeUuid", "roomId", "userName"),
                new OrderPlacedEvent("uuid", "pppp", "user"),
                new OrderingProcessClosedEvent("uuid", "userName")
        )
                .when(new CancelOrderCommand("uuid", "user"))
                .expectException(OrderingProcessClosedException.class);

    }

    @Test
    public void testFailingCancellingOrder() throws Exception {
        fixture.given(
                new OrderingProcessOpenedEvent("uuid", "placeUuid", "roomId", "userName"),
                new OrderPlacedEvent("uuid", "pppp", "user")
        )
                .when(new CancelOrderCommand("uuid", "noone"))
                .expectException(OrderDoesNotExistsException.class);
    }

    @Test
    public void testClosingOrderProcess() throws Exception {
        fixture.given(new OrderingProcessOpenedEvent("uuid", "placeUuid", "roomId", "userName"))
                .when(new CloseOrderingProcessCommand("uuid", "userName"))
                .expectVoidReturnType()
                .expectEvents(new OrderingProcessClosedEvent("uuid", "userName"));
    }

    @Test
    public void testCompletingOrder() throws Exception {
        Map<String, String> orders = new HashMap<>();
        orders.put("user", "pppp");
        orders.put("otheruser", "pppp");

        LocalDateTime dateTime = LocalDateTime.now();

        fixture.given(
                new OrderingProcessOpenedEvent("uuid", "placeUuid", "roomId", "userName"),
                new OrderPlacedEvent("uuid", "pppp", "user"),
                new OrderPlacedEvent("uuid", "pppp", "otheruser")
        )
                .when(new CompleteOrderingProcessCommand("uuid", dateTime, "userName"))
                .expectVoidReturnType()
                .expectEvents(new OrderingProcessCompletedEvent("uuid", "placeUuid", orders, dateTime, "userName"));
    }

    @Test
    public void testCompletingOrderProcessWithoutOrders() throws Exception {
        Map<String, String> orders = new HashMap<>();
        orders.put("user", "pppp");
        orders.put("otheruser", "pppp");

        fixture.given(new OrderingProcessOpenedEvent("uuid", "placeUuid", "roomId", "userName"))
                .when(new CompleteOrderingProcessCommand("uuid", LocalDateTime.now(), "userName"))
                .expectVoidReturnType()
                .expectEvents(new OrderingProcessClosedEvent("uuid", "userName"));
    }

    @Test
    public void testCompletingOrderProcessWhenStatusIsClosed() throws Exception {
        fixture.given(
                new OrderingProcessOpenedEvent("uuid", "placeUuid", "roomId", "userName"),
                new OrderPlacedEvent("uuid", "pppp", "user"),
                new OrderingProcessClosedEvent("uuid", "userName")
        )
                .when(new CompleteOrderingProcessCommand("uuid", LocalDateTime.now(), "userName"))
                .expectException(OrderingProcessClosedException.class);
    }

    @Test
    public void testClosingOrderingProcessByDifferentUser() throws Exception {
        fixture.given(new OrderingProcessOpenedEvent("uuid", "placeUuid", "roomId", "userName"))
                .when(new CloseOrderingProcessCommand("uuid", "differentUserName"))
                .expectException(NotTheSameUserException.class);
    }

    @Test
    public void testCompletingOrderByDifferentUser() throws Exception {
        fixture.given(
                new OrderingProcessOpenedEvent("uuid", "placeUuid", "roomId", "userName"),
                new OrderPlacedEvent("uuid", "pppp", "user"),
                new OrderPlacedEvent("uuid", "pppp", "otheruser")
        )
                .when(new CompleteOrderingProcessCommand("uuid", LocalDateTime.now(), "differentName"))
                .expectException(NotTheSameUserException.class);
    }

    @Test
    public void testCompletingOrderProcessWithoutOrdersByDifferentUser() throws Exception {
        fixture.given(new OrderingProcessOpenedEvent("uuid", "placeUuid", "roomId", "userName"))
                .when(new CompleteOrderingProcessCommand("uuid", LocalDateTime.now(), "differentName"))
                .expectException(NotTheSameUserException.class);
    }
}
