package com.darsadow.lunchbot.place;

import com.darsadow.lunchbot.coreapi.*;
import org.axonframework.test.aggregate.AggregateTestFixture;
import org.axonframework.test.aggregate.FixtureConfiguration;
import org.junit.Before;
import org.junit.Test;

public class PlaceTest {

    private FixtureConfiguration<Place> fixture;

    @Before
    public void setUp() {
        fixture = new AggregateTestFixture<>(Place.class);
    }

    @Test
    public void testPlaceCreation() {
        fixture.givenNoPriorActivity()
                .when(new CreatePlaceCommand("uuid", "accountuuid", "Place name"))
                .expectReturnValue("uuid")
                .expectEvents(new PlaceCreatedEvent("uuid", "accountuuid", "Place name"));
    }

    @Test
    public void testPlaceNameChange() throws Exception {
        fixture.given(new PlaceCreatedEvent("uuid", "accountUiid", "Test place"))
                .when(new ChangePlaceNameCommand("uuid", "New name"))
                .expectVoidReturnType()
                .expectEvents(new PlaceNameChangedEvent("uuid", "New name"));

    }

    @Test
    public void testAddNewMenuItem() throws Exception {
        fixture.given(new PlaceCreatedEvent("uuid", "accountUiid", "Test place"))
                .when(new SetMenuItemCommand("uuid", "pppp", "Pulled Pork Panini"))
                .expectVoidReturnType()
                .expectEvents(new MenuItemAddedEvent("uuid", "pppp", "Pulled Pork Panini"));

    }

    @Test
    public void testUpdateMenuItem() throws Exception {
        fixture.given(
                new PlaceCreatedEvent("uuid", "accountUiid", "Test place"),
                new MenuItemAddedEvent("uuid", "pppp", "Pulled Pork Panini")
        )
                .when(new SetMenuItemCommand("uuid", "pppp", "Pulled Pork Panini Please"))
                .expectVoidReturnType()
                .expectEvents(new MenuItemUpdatedEvent("uuid", "pppp", "Pulled Pork Panini Please"));

    }
}
