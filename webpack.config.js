var path = require('path');

var config = {
    context: path.join(__dirname, 'src/main/resources/ui'),
    entry: [
        './main.js'
    ],
    output: {
        path: path.join(__dirname, 'src/main/resources/static/js'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /src\/main\/resources\/ui\/.*\.js$/,
                exclude: /node_modules/,
                loaders: ['babel-loader']
            }
        ]
    },
    resolveLoader: {
        root: [
            path.join(__dirname, 'node_modules')
        ]
    },
    resolve: {
        root: [
            path.join(__dirname, 'node_modules')
        ]
    }
};
module.exports = config;
